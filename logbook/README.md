# Logbook (journal de bord) of your work

During this semester, you will not have to produce a final report.
However, a series of logbooks about the project are required.
Furthermore, this work must be done through the whole semester and is checked every week.

This document is also used to give more details of your work that you cannot put in your presentations.
The important idea behind it, is to have an accessible work that could be reused later.
Imagine that you are working on a public project that people could join later.

## Requirements:

You have to create a directory `logbook` in your GitLab project.
**Each week** you have to update this folder with some information of your project.
Create a single file that will contain all of your logbooks.
You can use the folder to insert images or other useful resources.
Do not overwrite your previous logbooks ! We want to keep all of them !

## What is logbook for us:

The logbook is a summary of your research work, which, like the presentation, will develop throughout the semester.
It aims to answer to questions such as:
What did you look at ?
What did you read ?
What did you test ?
What are your ideas ? Are they work ?
...


## What must this document contain ?

- The date

- What did you research ? Write down the resources that you consulted and that are relevant. Summarise each of them in two or three sentences maximum.

- What is/are your current objective(s) ?
Have you faced any issues ? Which ones ? Are they solved ?

- What did you produce ? Explain shortly how it solves your problems. Do not hesitate to upload pictures such as drawing, schemes, diagrams, etc...

- What is/are your next goal(s) ?  

- If you have other comments concerning the project, feel free to add them.

For the format, you can use what you want. However, it must be easy to read ! You can also use markdown if you like it.
You should be able to insert different kind of images.
