# Model Checking CTL on Petri nets  

**Maxime Lefranc** : maxime.lefranc@etu.unige.ch

**Maxime Rambosson** : maxime.rambosson@etu.unige.ch

## **Description** :

---

The project focuses on the exploration of the capability of checking CTL formulas with bounded Petri nets. The main objective is to investigate the kind of properties that can be proven through this approach and determine the limits of the methodology. The project also aims to determine the decidability of this approach and explore the existing tools available for this purpose. In addition to this, the project will involve creating examples to demonstrate how these proofs are computed. The Model Checking Contest is a vital resource that the project will use to find the best tools for model checking in different categories, including CTL forms executed on Petri nets. Ultimately, the project will aim to contribute to the advancement of the field of model checking and its application in various domains.  

<br>

## **Objectifs** :

---

The objectives of this project are:
- To explore the capability of checking CTL formulas with bounded Petri nets.
- To determine the kind of properties that can be proven through this approach and investigate the limits of the methodology.
- To determine the decidability of this approach.
- To explore the existing tools available for this purpose, particularly those in the CTL category of the Model Checking Contest.

<br>

## **Preliminary knowledge** :

---

For a complete understanding of this project, we assume that the readers have knowledge in the fields relating to our study, if this is not the case, a reminder of these notions is present on the report of our project. The concepts to know are related to Petri nets, CTL language and model checking.

<br>

## **Case studies** :

---

We decided to focus our study around the Tapaal software [1], however a slight comparison with the GreatSPN software [2] will be made.

<br>

## **Examples used** :

---

We used two examples :

**Dinner of the philosophers** : The dining philosophers problem is a well-known example in concurrent programming and demonstrates the challenge of avoiding deadlocks in resource allocation [3]. In our study, we use the dining philosophers problem as a case study to evaluate the capabilities and limitations of TAPAAL. We consider two scenarios: one with five philosophers and another with 50 philosophers.

**Building a house** : This example is directly proposed by Tapaal and models the construction of a house.


## **References** :

---

[1] tapaal.net : Introduction. (s. d.). https://www.tapaal.net/.  

[2] GreatSPN home page -  Dipartimento di Informatica - Università di Torino. (s. d.). http://www.di.unito.it/~greatspn/index.html.  

[3] Contributors to Wikimedia projects. (2023). Dinner of the philosophers. en.wikipedia.org. https://fr.wikipedia.org/wiki/D%C3%AEner_des_philosophes. 

